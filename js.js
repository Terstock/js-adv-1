// Теоретичне питання
// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//коли ми маємо об'єкт, який ми хочемо чатсково використати в інших об'єктах, то аби не повторювати код багато разів ми використовуємо власне
//прототипне наслідування - встановлюємо прототип на початковий об'єкт і якщо, звертаючись до нового об'єкту, такої властивості у нього немає, то
//програма бере ї з нашого прототипу - початкового об'єкту
// Для чого потрібно викликати super() у конструкторі класу-нащадка?
//взагалі ми використовуємо super для успадкування властивостей, але саме потрібно нам його викликати саме в тому випадку, якщо у нашого дочірнього класу
//є конструктор.

// Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.


class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    
    set name(newName){
        this._name = newName;
    }

    set age(newAge){
        this._age = newAge;
    }

    set salary(newSalary){
        this._salary = newSalary;
    }
    
    get name(){
        return " Name: " + this._name;
    }

    get age(){
        return " Age: " + this._age;
    }

    get salary(){
        return " Salary: " + this._salary;
    }
}

let worker = new Employee("Michail", 22, 1200)
console.log(worker);
console.log(worker.name + worker.age + worker.salary);

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang;
    }
    set salary(newSalary){
        this._salary = newSalary;
    }
    get salary(){
        return this._salary *3;
    }
}

let programmer = new Programmer("Teoden", 31, 1500, ["English","Ukrainian","German"])
console.log(programmer);

let programmer2 = new Programmer("Warris", 21, 400, ["Franch","Spanish","Japanish"])
console.log(programmer2.salary);